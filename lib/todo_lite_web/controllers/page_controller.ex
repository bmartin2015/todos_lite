defmodule TodoLiteWeb.PageController do
  use TodoLiteWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
