defmodule TodoLite.Repo do
  use Ecto.Repo,
    otp_app: :todo_lite,
    adapter: Ecto.Adapters.Postgres
end
