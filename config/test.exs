use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :todo_lite, TodoLiteWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :todo_lite, TodoLite.Repo,
  username: "postgres",
  password: "postgres",
  database: "todo_lite_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox
